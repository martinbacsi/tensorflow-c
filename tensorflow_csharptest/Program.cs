﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Tensortest
{
    public class Tensorflow
    {
        [DllImport("tensorwrap.dll")]
        public static extern void Init([MarshalAs(UnmanagedType.LPStr)] string fileName);

        [DllImport("tensorwrap.dll")]
        public static extern void Free();

        [DllImport("tensorwrap.dll")]
        public static extern void Run(IntPtr input, IntPtr output, int Width, int Height, int Channels);

        public void RunGraph<TColor>(Image<TColor, float> inputImg, Image<TColor, float> outImg)
        where TColor : struct, IColor
        {
            Run(inputImg.Mat.DataPointer, outImg.Mat.DataPointer, inputImg.Width, inputImg.Height, inputImg.NumberOfChannels);
        }

        public Tensorflow(string fileName)
        {
            Init(fileName);
        }

        ~Tensorflow()
        {
            Free();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //dll path
            Environment.SetEnvironmentVariable("PATH", Environment.GetEnvironmentVariable("PATH") + ";" + "../../../");
            Tensorflow tf = new Tensorflow("graph.pb");


            var odd = new Image<Gray, float>("odd.png");
            var even = new Image<Gray, float>("even.png");

            //var img = new Image<Gray, float>("../../../testecske.png");


            var merged = new Image<Gray, float>(new Image<Gray, float>[] { even, odd });



            var imgout = new Image<Gray, float>(128, 128);
            // while (true)
            {
                long now = DateTime.Now.Ticks;
                tf.RunGraph(merged, imgout);
                imgout.Save("output.png");

                Console.WriteLine((DateTime.Now.Ticks - now) * 0.0001);
            }
        }
    }
}
