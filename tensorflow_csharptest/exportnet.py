import tensorflow as tf
import numpy as np
from tensorflow.python.framework.graph_util import convert_variables_to_constants

def conv2d(x):

  return 


def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial)

with tf.Session() as sess:
    #input = np.array([[1.0, 2.0, 3.0], [3.0, 2.0, 1.0], [1.0, 2.0, 3.0]]).reshape([1, 3, 3, 1]).astype('float32')

    a = tf.placeholder(tf.float32, [1, 2048, 2048, 1], name='input')
    W = tf.Variable( tf.truncated_normal([3, 3, 1, 1], stddev=10.0))
    
    sess.run(tf.global_variables_initializer())

    

    b = tf.layers.max_pooling2d(tf.nn.conv2d(a, W, strides=[1, 1, 1, 1], padding='SAME'), 2, 2)

    c = tf.nn.conv2d(b, W, strides=[1, 1, 1, 1], padding='SAME'  , name = "output")
    print(sess.run(c, feed_dict = { a: np.zeros((1, 2048, 2048, 1))}).shape)
    #print(a.eval()) # 5.0
    #print(b.eval()  ) # 6.0

    

    minimal_graph = convert_variables_to_constants(sess, sess.graph_def, ["output"])

    tf.train.write_graph(minimal_graph, 'c:/models/', 'graph.pb', as_text=False)
    #tf.train.write_graph(sess.graph_def, 'c:/models/', 'graph.pb', as_text=False)